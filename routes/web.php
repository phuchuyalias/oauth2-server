<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/', 'OauthController@index');

/*
// First route that user visits on consumer app
Route::get('/', function () {
    // Build the query parameter string to pass auth information to our request
    $query = http_build_query([
        'client_id' => 3,
        'redirect_uri' => 'https://script.google.com/macros/d/11E18OAQUH6EayJUvUH2iqYnvIygasSFJZIKzvFy4J2XmuuuO3gp3VI7k/usercallback',
        'response_type' => 'code',
        'scope' => 'conference'
    ]);

    // Redirect the user to the OAuth authorization page
    return redirect('http://api.dragit.net/oauth/authorize?' . $query);
});
*/

// Route that user is forwarded back to after approving on server
Route::get('callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://api.dragit.net/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => 4, // from admin panel above
            'client_secret' => 'bak1juLt017jJXtTgOX4EHa8k9Z9kGkaYU6YTo1o', // from admin panel above
            'redirect_uri' => 'https://script.google.com/macros/d/MUsk3PzftH89PHx8DLAIm9kXuNCv9T6dc/usercallback',
            //'redirect_uri' => 'https://script.google.com/macros/d/15qoNAmyJQI5KK-RjNu04gYZytPk7rLI-uIeFR4TszR6axq_Zs83gaa3W/usercallback',
            'code' => $request->code // Get code from the callback
        ]
    ]);
    // echo the access token; normally we would save this in the DB
    return json_decode((string) $response->getBody(), true)['access_token'];
});
Route::match(array('post', 'get'), 'api/login', 'TestController@login');
Route::match(array('post', 'get'), 'api/images', 'TestController@image');
Route::match(array('post', 'get'), 'api/background', 'TestController@background');
Route::match(array('post', 'get'), 'api/quotes', 'TestController@quotes');
Route::match(array('post', 'get'), 'api/images/{id}', 'TestController@detail');
Route::match(array('post', 'get'), 'api/summary', 'TestController@summary')->middleware('auth:api');
Route::match(array('post', 'get'), 'api/buy', 'TestController@buy')->middleware('auth:api');
Route::match(array('post', 'get'), 'api/refresh', 'TestController@refresh');


