<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use joshtronic\LoremIpsum;
use Image;

class TestController extends Controller {

     /*
     |--------------------------------------------------------------------------
     | Home Controller
     |--------------------------------------------------------------------------
     |
     | This controller renders your application's "dashboard" for users that
     | are authenticated. Of course, you are free to change or remove the
     | controller as you wish. It is just here to get your app started!
     |
     */

     /**
      * Create a new controller instance.
      *
      * @return void
      */
     public function __construct()
     {
     }

     /**
      * Show the application dashboard to the user.
      *
      * @return Response
      */
     public function index()
     {
          return view('home');
     }
     function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
     /**
      * Show the application dashboard to the user.
      *
      * @return Response
      */
     public function login(Request $request)
     {
          $username = $request->input('username');
          $password = $request->input('password');
          if(empty($username) || empty($password)){
              return response(array('Missing username or password'), 203)
                    ->header('Content-Type', 'application/json');
          }
          else{
              if($username == '123'){
                  $json = array(
                      'error' => 'Wrong username or password',
                  );
              }
              else{
                  $json = array(
                      'name' => 'Anonymous',
                      'uid' => rand(1,1000),
                      'secretkey' => $this->generateRandomString(20),
                      'token' => $this->generateRandomString(40),
                      'expiry' => strtotime("+5 minutes")
                  );
              }
              return response($json, 200)
                 ->header('Content-Type', 'application/json');
          }

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function image(Request $request)
     {
          $lipsum = new LoremIpsum();
          $keyword = $request->input('keyword');
          $limit = $request->input('limit', 10);
          $offset = $request->input('offset', 0);
          $json = array();
          if($offset > 200) {
              return response(array(), 200)
                 ->header('Content-Type', 'application/json');
          }
          for ($i=0; $i < $limit ; $i++) {
             $name = 'image ('.rand(1,24).').jpg';
             if (!\File::exists(public_path('images/thumbnails/'.$name)))
             {
                 $image = Image::make(public_path('images/'.$name));
                 $image->fit(100, 100)->save(public_path('images/thumbnails/'. $name));
             }
             $json[] = array(
                 'id' => $offset + $i,
                 'name' => $lipsum->words(5),
                 'caption' => $lipsum->sentences(3),
                 'thumbnail' => url('/').'/images/thumbnails/'.$name,
                 'price' => rand(1,10),
                 'url' => url('/').'/images/'.$name
             );
          }
          return response($json, 200)
             ->header('Content-Type', 'application/json');

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function background(Request $request)
     {
          $lipsum = new LoremIpsum();
          $keyword = $request->input('keyword');
          $limit = $request->input('limit', 0);
          $offset = $request->input('offset', 0);
          $total = $limit > 0 ? $limit: 14;
          $json = array();
          if($offset > 200) {
              return response(array(), 200)
                 ->header('Content-Type', 'application/json');
          }
          for ($i=1; $i < $total ; $i++) {
             $name = 'bg'.$i.'.jpg';
             $json[] = array(
                 'id' => $offset + $i,
                 'name' => $lipsum->words(5),
                 'author' => $lipsum->words(3),
                 'caption' => $lipsum->sentences(3),
                 'url' => url('/').'/images/background/'.$name
             );
          }
          return response($json, 200)
             ->header('Content-Type', 'application/json');

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function quotes(Request $request)
     {
          $lipsum = new LoremIpsum();
          $json = array();
          for ($i=1; $i < 10 ; $i++) {
             $json[] = array(
                 'id' => $i,
                 'author' => $lipsum->words(5),
                 'quote' => $lipsum->sentences(3)
             );
          }
          return response($json, 200)
             ->header('Content-Type', 'application/json');

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function detail($id, Request $request)
     {
          $lipsum = new LoremIpsum();
          $json = array(
             'id' => $id,
             'name' => $lipsum->words(5),
             'description' => $lipsum->sentences(3),
             'copyright' => $lipsum->words(3),
             'keywords' => $lipsum->words(5),
             'price' => rand(1,10),
             'url' => url('/').'/images/image ('.rand(1,24).').jpg'
         );
          return response($json, 200)
             ->header('Content-Type', 'application/json');

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function summary(Request $request)
     {
          $lipsum = new LoremIpsum();
          $json = array(
             'name' => $lipsum->words(5),
             'subscription' => rand(1,5),
             'credit' => rand(200, 7000)
         );
          return response($json, 200)
             ->header('Content-Type', 'application/json');

     }
     /**
      * get sample images
      *
      * @return Response
      */
     public function buy(Request $request)
     {
         $lipsum = new LoremIpsum();
         $id = $request->input('id');
         if(empty($id)){
             return response(array('Missing image id'), 203)
                   ->header('Content-Type', 'application/json');
         }
         $flag = rand(0,1);
         if($flag){
             $json = array(
                'id' => $id,
                'name' => $lipsum->words(5),
                'description' => $lipsum->sentences(3),
                'copyright' => $lipsum->words(3),
                'keywords' => $lipsum->words(5),
                'message' => 'Buy image successful and add',
                'url' => url('/').'/images/image ('.rand(1,24).').jpg',
            );
         }
         else{
             return response(array('User can not buy have to subscribe for subscriptions'), 203)
                   ->header('Content-Type', 'application/json');
         }
         return response($json, 200)
            ->header('Content-Type', 'application/json');

     }
     /**
      * Show the application dashboard to the user.
      *
      * @return Response
      */
     public function refresh(Request $request)
     {
          $token = $request->input('token');
          $uid = $request->input('uid');
          if(empty($uid) || empty($token)){
              return response(array('Missing token or secret key or uid'), 203)
                    ->header('Content-Type', 'application/json');
          }
          else{
              $json = array(
                  'token' => $this->generateRandomString(40),
                  'expiry' => strtotime("+5 minutes")
              );
              return response($json, 200)
                 ->header('Content-Type', 'application/json');
          }

     }
}
