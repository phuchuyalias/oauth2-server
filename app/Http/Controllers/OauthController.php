<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OauthClient;

class OauthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $client_id = $request->input('client_id');
        $response_type = $request->input('response_type');
        $redirect_uri = OauthClient::find($client_id)->redirect;


        $query = http_build_query([
            'client_id' => $client_id,
            'redirect_uri' => $redirect_uri,
            'response_type' =>  $response_type,
            'scope' => 'conference'
        ]);

        return redirect('http://api.dragit.net/oauth/authorize?' . $query);
    }
}
